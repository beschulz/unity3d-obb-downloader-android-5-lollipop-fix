# Unity3d OBB Downloader Android 5 Lollipop Fix

This is a fix for the [Unity OBB Downloader](http://u3d.as/content/unity-technologies/google-play-obb-downloader/2Qq) by Unity Technologies.

This fixes the "Caused by: java.lang.IllegalArgumentException: Service Intent must be explicit" that started appearing in Android 5 Lollipop.
This is nothing more than a copy of a basic Unity Project with the OBB Downloader Plugin from the Asset store.
The BugFix was done in the java-part (LicenseChecker.java) and is nothing more than a copy-paste fix with the help of
https://gist.github.com/SeanZoR/068f19545e51e4627749

The repository is kind of a mess. But if you stumble upon this repository, you're most likely interested in the compiled and obfuscated .jar file at
Assets/Plugins/Android/bin/obfuscated_googleplaydownloader.jar

Just install the plugin from the asset store and replace obfuscated_googleplaydownloader.jar with the one from this repository.

Take care, when installing the plugin from the asset store though, I have the feeling, that the AndroidManifest is not propperly merged.

Do not forget to paste your public key into Assets/Plugins/Android/GooglePlayDownloader.cs
